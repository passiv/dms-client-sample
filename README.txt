This is a sample web app to consume DMS webservice.

to run this app you need to :
1. import certificate to java certificate store
	for example run this command in Windows cmd (change paths) :
	keytool.exe -import -storepass "changeit" -keystore "C:\Program Files\Java\jdk-11\lib\security\cacerts" -alias certificate.cer -file "C:\Users\tprit\Desktop\2chart_cert\www.dev.2charta-cloud.de.cer" -noprompt
	
	for linux (change paths) :
	keytool -import -storepass "changeit" -keystore "/usr/lib/jvm/java-11-openjdk-amd64/lib/security/cacerts" -alias certificate.cer -file "/git/www.dev.2charta-cloud.de.cer" -noprompt

2. build the application (for both Linux and Windows)
	mvn clean install

3. run the application with Maven (for both Linux and Windows):
	mvn spring-boot:run
	
4. app is available at : http://localhost/index.xhtml

feel free to contact me at : t.pritrsky@gmail.com
