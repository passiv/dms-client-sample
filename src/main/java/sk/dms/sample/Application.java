package sk.dms.sample;

import org.ocpsoft.rewrite.servlet.RewriteFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

import javax.faces.webapp.FacesServlet;
import javax.servlet.DispatcherType;
import javax.servlet.MultipartConfigElement;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);

        Map<String, Object> properties = new HashMap<>();
        properties.put("server.port", "80");

        // disable 'Restart' classloader - solves problem when Spring uses different ClassLoader for *ServicePort method
        properties.put("spring.devtools.restart.enabled", "false");
        System.setProperty("spring.devtools.restart.enabled", "false");

        app.setDefaultProperties(properties);

        app.run(args);

        System.out.println("****************");
        System.out.println("'DMS Client Sample' application successfully started at :");
        System.out.println("http://localhost:80/index");
        System.out.println("****************");
    }

    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        FacesServlet servlet = new FacesServlet();
        return new ServletRegistrationBean(servlet, "*.jsf", "*.xhtml");
    }

    @Bean
    public FilterRegistrationBean rewriteFilter() {
        FilterRegistrationBean rwFilter = new FilterRegistrationBean(new RewriteFilter());
        rwFilter.setDispatcherTypes(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST,
                DispatcherType.ASYNC, DispatcherType.ERROR));
        rwFilter.addUrlPatterns("/*");
        return rwFilter;
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(DataSize.of(128l, DataUnit.MEGABYTES));
        factory.setMaxRequestSize(DataSize.of(128l, DataUnit.MEGABYTES));
        return factory.createMultipartConfig();
    }

}
