package sk.dms.sample;

import org.ocpsoft.rewrite.el.ELBeanName;
import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope(value = "session")
@Component(value = "uploadTest")
@ELBeanName(value = "uploadTest")
public class UploadTest {

    private UploadedFile file = null;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void test() {
        System.out.println();
    }

    public void test2() {
        System.out.println();
    }

}
