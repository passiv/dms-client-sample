package sk.dms.sample.bootfaces.home;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope(value = "view")
@Component(value = "indexController")
@ELBeanName(value = "indexController")
@Join(path = "/index", to = "/index.jsf")
public class IndexController {
}
