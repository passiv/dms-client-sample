package sk.dms.sample.bootfaces.wsdl;

import javax.activation.DataSource;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileDataSource implements DataSource {

    public static final String UPLOADS_PATH = System.getProperty("user.dir") + File.separator + "upload";

    private Path uploadedFile;
    private String contentType;

    public FileDataSource(Path uploadedFile, String contentType) {
        this.uploadedFile = uploadedFile;
        this.contentType = contentType;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return Files.newInputStream(uploadedFile);
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return Files.newOutputStream(Paths.get(FileDataSource.UPLOADS_PATH + File.separator + uploadedFile.getFileName()));
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public String getName() {
        return uploadedFile.getFileName().toString();
    }
}
