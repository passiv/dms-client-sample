package sk.dms.sample.bootfaces.wsdl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.ws.fault.ServerSOAPFaultException;
import org.apache.commons.lang3.StringUtils;
import org.oasis_open.docs.ns.cmis.core._200908.*;
import org.oasis_open.docs.ns.cmis.messaging._200908.CmisContentStreamType;
import org.oasis_open.docs.ns.cmis.messaging._200908.CmisObjectInFolderContainerType;
import org.oasis_open.docs.ns.cmis.messaging._200908.CmisRepositoryEntryType;
import org.oasis_open.docs.ns.cmis.ws._200908.*;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.shaded.commons.io.FilenameUtils;
import sk.dms.sample.logging.LogWrapper;
import sk.dms.sample.logging.LoggedFunction;

import javax.activation.DataHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.BindingProvider;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;

@ManagedBean
@RequestScoped
public class WsdlTest implements Serializable {

    private String repositoryId = "200_5";
    private String folderId;
    private String result;
    private String consoleOutput;

    private UploadedFile file;
    private List<CmisProperty> createDocumentProperties = new ArrayList<>();
    private EnumVersioningState versioningState;
    private String createDocumentFolderId;
    private String policies;
    private List<CmisAccessControlEntryType> cmisAccessControlEntryTypes = new ArrayList<>();
    private String permissions;

    static {
        // log SOAP request/response
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");
    }

    public WsdlTest() {
//        cmisPropertyList.add(new CmisProperty());
//        this.onAddNewCmisAccessControlEntryTypes();
    }

    public List<CmisRepositoryEntryType> getRepositories() {
        final List<CmisRepositoryEntryType> result = new LinkedList<>();

        LoggedFunction lf = new LoggedFunction() {
            @Override
            public void execute() {
                List<CmisRepositoryEntryType> webserviceResult = callGetRepositories();
                if (webserviceResult != null) {
                    result.addAll(webserviceResult);
                }
            }
        };

        this.consoleOutput = LogWrapper.executeLoggedFunction(lf);

        ObjectMapper o = new ObjectMapper();
        try {
            this.result = o.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to deserialize response", e);
        }
        return result;
    }

    private List<CmisRepositoryEntryType> callGetRepositories() {
        List<CmisRepositoryEntryType> result = null;
        System.setProperty("javax.xml.soap.SAAJMetaFactory", "com.sun.xml.messaging.saaj.soap.SAAJMetaFactoryImpl");
        RepositoryService repositoryService = new RepositoryService();
        try {
            RepositoryServicePort repositoryServicePort = repositoryService.getRepositoryServicePort();
            setUserCredentials((BindingProvider) repositoryServicePort);

            result = repositoryServicePort.getRepositories(null);
        } catch (ServerSOAPFaultException | CmisException e) {
            this.result = e.getLocalizedMessage();
        } catch (Exception e1) {
            throw new RuntimeException("Webservice call failed", e1);
        }
        return result;
    }

    public void getFolderTree() {
        this.result = null;
        LoggedFunction lf = new LoggedFunction() {
            @Override
            public void execute() {
                callGetFolderTree();
            }
        };
        this.consoleOutput = LogWrapper.executeLoggedFunction(lf);
    }

    private void callGetFolderTree() {
        System.setProperty("javax.xml.soap.SAAJMetaFactory", "com.sun.xml.messaging.saaj.soap.SAAJMetaFactoryImpl");
        NavigationService navigationService = new NavigationService();
        try {
            NavigationServicePort navigationServicePort = navigationService.getNavigationServicePort();
            setUserCredentials((BindingProvider) navigationServicePort);
            List<CmisObjectInFolderContainerType> successfulResponse = navigationServicePort.getFolderTree(repositoryId, null, new BigInteger("3"), null, Boolean.FALSE, EnumIncludeRelationships.NONE, null, Boolean.FALSE, null);
            this.result = this.printAsJson(successfulResponse);
        } catch (ServerSOAPFaultException e) {
            this.result = e.getMessage() + "<br/>" + printAsJson(e.getFault());
            e.printStackTrace();
        } catch (CmisException e) {
            this.result = e.getMessage() + "<br/> " + printAsJson(e.getFaultInfo());
            e.printStackTrace();
        } catch (Exception e) {
            throw new RuntimeException("Webservice call failed", e);
        }
    }

    public void createFolder(String folderId) {
        this.result = null;
        LoggedFunction lf = new LoggedFunction() {
            @Override
            public void execute() {
                callCreateFolder(folderId);
            }
        };
        this.consoleOutput = LogWrapper.executeLoggedFunction(lf);
    }

    private void callCreateFolder(String folderId) {
        System.setProperty("javax.xml.soap.SAAJMetaFactory", "com.sun.xml.messaging.saaj.soap.SAAJMetaFactoryImpl");
        ObjectService objectService = new ObjectService();
        try {
            ObjectServicePort objectServicePort = objectService.getObjectServicePort();
            setUserCredentials((BindingProvider) objectServicePort);

            CmisPropertiesType properties = new CmisPropertiesType();

            objectServicePort.createFolder(repositoryId, properties, folderId, null, null, null, null, null);
            this.result = "Success";
        } catch (ServerSOAPFaultException e) {
            this.result = e.getMessage() + "<br/>" + printAsJson(e.getFault());
            e.printStackTrace();
        } catch (CmisException e) {
            this.result = e.getMessage() + "<br/> " + printAsJson(e.getFaultInfo());
            e.printStackTrace();
        } catch (Exception e) {
            throw new RuntimeException("Webservice call failed", e);
        }
    }

    public void uploadDocument(FileUploadEvent event) {
        this.file = event.getFile();
        createDocument();
    }

    public void createDocument() {
        this.result = null;

        LoggedFunction lf = new LoggedFunction() {
            @Override
            public void execute() {
                callCreateDocument(file);
            }
        };
        this.consoleOutput = LogWrapper.executeLoggedFunction(lf);
    }

    private void callCreateDocument(UploadedFile file) {
        if (file == null) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Please upload a file first."));
            return;
        }

        Path tempFile = null;

        System.setProperty("javax.xml.soap.SAAJMetaFactory", "com.sun.xml.messaging.saaj.soap.SAAJMetaFactoryImpl");
        ObjectService objectService = new ObjectService();
        try {
            ObjectServicePort objectServicePort = objectService.getObjectServicePort();
            setUserCredentials((BindingProvider) objectServicePort);

            String filename = FilenameUtils.getBaseName(file.getFileName());
            String extension = FilenameUtils.getExtension(file.getFileName());
            tempFile = Files.createTempFile(Path.of(FileDataSource.UPLOADS_PATH), filename + "-", "." + extension);

            Files.copy(file.getInputstream(), tempFile, StandardCopyOption.REPLACE_EXISTING);

            CmisContentStreamType stream = new CmisContentStreamType();
            stream.setFilename(file.getFileName());
            stream.setLength(new BigInteger("" + file.getSize()));
            stream.setMimeType(file.getContentType());

            CmisPropertiesType properties = new CmisPropertiesType();
            properties.getPropertyBooleanOrPropertyIdOrPropertyInteger().addAll(getCmisPropertyList());

            List<String> policiesParam = new ArrayList<>();
            if (!StringUtils.isEmpty(this.policies)) {
                policiesParam.addAll(Arrays.asList(this.policies.split("/r/n")));
            }

            CmisAccessControlListType cmisAccessControlListType = new CmisAccessControlListType();
            cmisAccessControlListType.getPermission().addAll(cmisAccessControlEntryTypes);

            stream.setStream(new DataHandler(new FileDataSource(tempFile, extension)));
            objectServicePort.createDocument(repositoryId, properties, getCreateDocumentFolderId(), stream,
                    getVersioningState(), policiesParam, null, null, null, null);

            this.result = "Success";
        } catch (ServerSOAPFaultException e) {
            this.result = e.getMessage() + "<br/>" + printAsJson(e.getFault());
            e.printStackTrace();
        } catch (CmisException e) {
            this.result = e.getMessage() + "<br/> " + printAsJson(e.getFaultInfo());
            e.printStackTrace();
        } catch (Exception e) {
            throw new RuntimeException("Webservice call failed", e);
        } finally {
            if (tempFile != null) {
                try {
                    Files.deleteIfExists(tempFile);
                } catch (IOException e) {
                    throw new RuntimeException("Failed to delete temporary file", e);
                }
            }
        }
    }

    private String printAsJson(Object o) {
        ObjectMapper om = new ObjectMapper();
        try {
            return om.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to print JSON", e);
        }
    }

    private void setUserCredentials(BindingProvider objectServicePort) {
        // use this to set webservice endpoint address (useful when WSDL is not generated via HTTPS, but is downloaded locally - better approach)
        // ((BindingProvider) navigationService).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serviceURL.toString());

        // set username/password
        objectServicePort.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "e21cmis");
        objectServicePort.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "ekom21123");
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getConsoleOutput() {
        return consoleOutput;
    }

    public void setConsoleOutput(String consoleOutput) {
        this.consoleOutput = consoleOutput;
    }

    public List<CmisProperty> getCreateDocumentProperties() {
        return createDocumentProperties;
    }

    public void setCreateDocumentProperties(List<CmisProperty> createDocumentProperties) {
        this.createDocumentProperties = createDocumentProperties;
    }

    final List<CmisProperty> cmisPropertyList = new ArrayList<>();

    public List<CmisProperty> getCmisPropertyList() {
        return cmisPropertyList;
    }

    public void onAddNewCmisProperty() {
        this.cmisPropertyList.add(new CmisProperty());
    }

    public EnumVersioningState getVersioningState() {
        return versioningState;
    }

    public void setVersioningState(EnumVersioningState versioningState) {
        this.versioningState = versioningState;
    }

    public EnumVersioningState[] getVersioningStates() {
        return EnumVersioningState.values();
    }

    public String getCreateDocumentFolderId() {
        return createDocumentFolderId;
    }

    public void setCreateDocumentFolderId(String createDocumentFolderId) {
        this.createDocumentFolderId = createDocumentFolderId;
    }

    public String getPolicies() {
        return policies;
    }

    public void setPolicies(String policies) {
        this.policies = policies;
    }

    public List<CmisAccessControlEntryType> getCmisAccessControlEntryTypes() {
        return cmisAccessControlEntryTypes;
    }

    public void setCmisAccessControlEntryTypes(List<CmisAccessControlEntryType> cmisAccessControlEntryTypes) {
        this.cmisAccessControlEntryTypes = cmisAccessControlEntryTypes;
    }

    public void onAddNewCmisAccessControlEntryTypes() {
        CmisAccessControlEntryType entryType = new CmisAccessControlEntryType();
        entryType.setPrincipal(new CmisAccessControlPrincipalType());
        entryType.setDirect(true);
        entryType.getPermission().add("testPermission1");
        entryType.getPermission().add("testPermission2");

        permissionMap.put(entryType, "");
        this.cmisAccessControlEntryTypes.add(entryType);
    }

    Map<CmisAccessControlEntryType, String> permissionMap = new HashMap<>();

    public Map<CmisAccessControlEntryType, String> getPermissionMap() {
        return permissionMap;
    }

    public void setPermissionMap(Map<CmisAccessControlEntryType, String> permissionMap) {
        this.permissionMap = permissionMap;
    }

    public String getAccessPermissions(CmisAccessControlEntryType type) {
        StringBuilder sb = new StringBuilder();

        for (String p : type.getPermission()) {
            sb.append(p);
            sb.append("\r\n");
        }
        return sb.toString();
    }

    public void setAccessPermissions(CmisAccessControlEntryType type, String permissions) {
        for (String p : permissions.split("\r\n")) {
            if (!StringUtils.isEmpty(p.trim())) {
                type.getPermission().remove(p);
                type.getPermission().add(p);
            }
        }
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public void submitCreateDocument() {
        System.out.println("submitting document");
        this.createDocument();
    }

    public String permissionMapGet(CmisAccessControlEntryType t) {
        return this.permissionMap.get(t);
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
}
