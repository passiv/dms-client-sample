package sk.dms.sample.logging;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public class LogWrapper {

    public static String executeLoggedFunction(LoggedFunction loggedFunction) {
        // Create a stream to hold the output
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        // IMPORTANT: Save the old System.out!
        PrintStream old = System.out;
        // Tell Java to use your special stream
        System.setOut(ps);

        loggedFunction.execute();

        // Put things back
        System.out.flush();
        System.setOut(old);
        // Show what happened
        System.out.println(baos.toString());

        String unescapedResult = new String(baos.toByteArray(), StandardCharsets.UTF_8);
        unescapedResult = unescapedResult.replaceAll("<", "&lt;");
        unescapedResult = unescapedResult.replaceAll(">", "&gt;");
        unescapedResult = unescapedResult.replaceAll("\r\n", "<br/>");

        return unescapedResult;
    }
}
