package sk.dms.sample.logging;

public interface LoggedFunction {

    void execute();
}
